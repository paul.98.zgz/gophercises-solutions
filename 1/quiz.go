////
//  EXERCISE 1 OF Gophercises
//  https://gophercises.com/exercises/quiz
//
//  Author: Paul Hodgetts
//

package main

import (
    "bufio"
    "encoding/csv"
    "fmt"
    "io"
    "log"
    "os"
    "sort"
    "strings"
    "time"
)

// Path of the quiz questions and solutions
var quizPath = "problems.csv"

// Number of correct answers
var correct = 0

// Number of total questions
var totalQuestions = 0

// Map of questions and solutions
var quiz = make(map[string]string)

// Condition for an ordered quiz
var ordered = true

// Time limit in seconds
const timeLimit = 30

////
//  Checks the arguments given to the program.
//  Expected arguments are:
//    * -f <file>: Specify source of the quiz. If not present, it will be set to the default value.
//    * -s: Shuffle the questions
////
func checkArgs() {

    if len(os.Args) == 3 && os.Args[1] == "-f" {
        quizPath = os.Args[2]
    } else if len(os.Args) == 2 && os.Args[1] == "-s" {
        ordered = false
    } else if len(os.Args) != 1 {
        fmt.Println("Unknown options: ", os.Args)
        os.Exit(0)
    }
}

////
//  Prints final message with the score of the quiz
////
func end()  {
    fmt.Printf("You scored %d out of %d.\n", correct, totalQuestions)
}

////
//  Prompts a message to the user to start a new game. Then, it sets a time limit for the game.
////
func setTimer() {
    fmt.Print("Press enter to start...")
    reader := bufio.NewReader(os.Stdin)
    _, _ = reader.ReadString('\n')

    timer := time.NewTimer(time.Second*timeLimit)

    go func() {
        <-timer.C
        fmt.Println("\nTime is up!")
        end()
        os.Exit(0)
    }()

}

////
//  Reads the source file of the quiz and sets it's content to a map.
////
func parseQuizFile() {
    csvFile, _ := os.Open(quizPath)
    readerFile := csv.NewReader(bufio.NewReader(csvFile))

    for {
        line, err := readerFile.Read()
        if err == io.EOF {
            break
        } else if err != nil {
            log.Fatal(err)
        }

        question := line[0]
        quiz[question] = strings.ToLower(line[1])
    }

    totalQuestions = len(quiz)
}

////
//  Given a quiz map, prompts questions to the user that must be answered.
////
func startQuiz() {
    readerInput := bufio.NewReader(os.Stdin)

    iProblem := 1

    keys := make([]string, 0)
    for k := range quiz {
        keys = append(keys, k)
    }

    if ordered {
        sort.Strings(keys)
    }

    for _, question := range keys {
        solution := quiz[question]

        fmt.Printf("Problem #%d: %s = ", iProblem, question)
        answer, _ := readerInput.ReadString('\n')
        answer = strings.Replace(answer, "\n", "", -1)
        answer = strings.ToLower(answer)
        answer = strings.TrimSpace(answer)

        if answer == solution {
            correct++
        }

        iProblem++
    }

    end()
}

////
//  Given a quiz map, prompts questions to the user that must be answered before the time is up.
//  See checkArgs() documentation to know available options.
////
func main() {
    checkArgs()
    parseQuizFile()
    setTimer()
    startQuiz()
}